// here we will hold the values;
var map, pos, places = new Array();

// Initialize the map (we do not show the map, just use coordinates it gives us)
function initMap(multi = false, sort = false) {
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: -34.397, lng: 150.644},
		zoom: 6
	});

	// Try HTML5 geolocation.
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			map.setCenter(pos);
			getATMs(pos, map, multi, sort);

		}, function() {
			handleLocationError(true);
		});
	} else {
		// Browser doesn't support Geolocation
		handleLocationError(false);
	}
}

function handleLocationError(browserHasGeolocation) {
	alert(browserHasGeolocation ?
		'Error: The Geolocation service failed. Please refresh and allow location usage!' :
		'Error: Your browser doesn\'t support geolocation.');
}

// get the nearest ten ATM's
function getATMs(pos, map, multi, sort) {
	var myLocation = new google.maps.LatLng(pos.lat,pos.lng)
	var request = {
		location: myLocation,
		radius: ((sort) ? '' : '5000'),
		type: ['atm'],
		rankBy: ((sort) ? google.maps.places.RankBy.DISTANCE : google.maps.places.RankBy.NAME),
		name: ((multi) ? "Telenor Banka" : '')
	};

	placesService = new google.maps.places.PlacesService(map);
	placesService.nearbySearch(request, function (results, status) {
		if (status == google.maps.places.PlacesServiceStatus.OK) {

			for (var i = 0; i < 10; i++) {
				places[i] = results[i];
				getDistance(myLocation, places[i])
					.then(function(place) {
						showOutput(place);
					})
					.catch(function(error) {
						alert(error);						
					})
			}
		} else {
			alert('Google search for places failed, please try again!');
		}
	});
}

// get distance for a found place
function getDistance(myLocation, place) {
	return new Promise(function(resolve, reject) {

		var distanceService = new google.maps.DistanceMatrixService();

		distanceService.getDistanceMatrix(
		{
			origins: [myLocation],
			destinations: [place.geometry.location],
			travelMode: 'WALKING',
		}, function (response, status) {
			if (status == 'OK') {
				var distance = response.rows[0].elements[0].distance;
				place['distance'] = distance;
				resolve(place);
			}
		});
	})
}
// print the results on the page
function showOutput(place) {
	var list_item = document.createElement("li");
	var img = document.createElement("img");
	var div = document.createElement("div");
	var h2 = document.createElement("h2");
	var h3 = document.createElement("h3");

	img.setAttribute('class', 'place__image');
	img.setAttribute('src', 'https://maps.googleapis.com/maps/api/staticmap?center=' + place.vicinity + '&zoom=13&scale=1&size=600x300&maptype=roadmap&format=png&visual_refresh=true');
	img.setAttribute('alt', place.vicinity);

	list_item.setAttribute('class', 'place');
	h2.setAttribute('class', 'place__title');
	h3.setAttribute('class', 'place__distance');
		
	h2.innerHTML = place.name;
	h3.innerHTML += place.distance.text;
	
	list_item.appendChild(div);
	div.appendChild(h2);
	list_item.appendChild(h3);
	list_item.appendChild(img);
	document.getElementById("atms").appendChild(list_item);
}

// onclick function for multy-currency ATMs
function multiPlaces(e) {
	document.getElementById("atms").innerHTML = '';
	if(e.checked == true) {
		if(document.getElementById('sort').checked == true) {
			// if multi-currency and sort applied
			initMap(true, true);
			document.getElementById('sort').checked = false;
		}
		else {
			initMap(true, false);
		}
	} else {
		if(document.getElementById('sort').checked == true) {
			document.getElementById('sort').checked = false;
		}
		initMap();
	}
}
// extend array's prototype with clone function to keep the original object for sorting
Array.prototype.clone = function() {
	return this.slice(0);
};

function sortPlaces(e) {
	document.getElementById('atms').innerHTML = '';
	// clone for toggling sorted places
	var sortedPlaces = places.clone();
		sortedPlaces.sort(compare);
	if(e.checked == true) {
		for (var i = 0; i < places.length; i++) {
			showOutput(sortedPlaces[i]);
		}
	} else {
		for (var i = 0; i < places.length; i++) {
			showOutput(places[i]);
		}	
	}
}

// usage: array.sort(compare);
function compare(a,b) {
  if (a.distance.value < b.distance.value)
    return -1;
  if (a.distance.value > b.distance.value)
    return 1;
  return 0;
}